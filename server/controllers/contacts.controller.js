/**
 * Created by Raz on 11/5/2017.
 */
var config = require('../config.json');
var express = require('express');
var router = express.Router();
var contactsService = require('../services/contacts.service.js');

router.get('/getAll', getAll);
router.post('/displayByFilter', getFilteredContacts);
router.post('/add', addContact);
router.post('/edit/:_id', editContact);
router.delete('/delContact/:_id', delContact);
router.delete('/delAll', delAll);

function getAll(req, res) {
    console.log("getAll");
    contactsService.getAll()
        .then(function (contacts) {
            res.send(contacts);
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function delAll(req, res) {
    console.log("delAll");
    contactsService.delAll()
        .then(function () {
            res.sendStatus(200);
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function getFilteredContacts(req, res) {
    console.log("getFilteredContacts");
    contactsService.getFilteredContacts(req.body.filter)
        .then(function (contacts) {
            res.send(contacts);
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function addContact(req, res) {
    console.log("addContact");
    contactsService.addContact(req.body)
        .then(function (contact) {
            res.send(contact);
        })
        .catch(function (err) {
            res.status(400).json(err);
        });
}

function editContact(req, res) {
    console.log("editContact");
    contactsService.editContact(req.params._id, req.body)
        .then(function (contact) {
            res.send(contact);
        })
        .catch(function (err) {
            res.status(400).json(err);
        });
}

function delContact(req, res) {
    console.log("delContact");
    contactsService.delContact(req.params._id)
        .then(function () {
            res.sendStatus(200);
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}



module.exports = router;
