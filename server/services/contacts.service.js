var config = require('../config.json');
var _ = require('lodash');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var Q = require('q');
var mongo = require('mongoskin');
var db = mongo.db(config.connectionString, { native_parser: true });
db.bind('contacts');

var service = {};

service.getAll = getAll;
service.delAll = delAll;
service.getFilteredContacts = getFilteredContacts;
service.addContact = addContact;
service.editContact = editContact;
service.delContact = delContact;

function getAll() {
    var deferred = Q.defer();
    db.contacts.find().toArray(function (err, contacts) {
        if (err) deferred.reject(err.name + ': ' + err.message);
        deferred.resolve(contacts);
    });

    return deferred.promise;
}

function delAll() {
    var deferred = Q.defer();

    db.contacts.remove({}, function(){
        deferred.resolve();
    });

    return deferred.promise;
}

function getFilteredContacts(filter) {
    var deferred = Q.defer();
    db.contacts.find({ name: { $regex: ".*" + filter + ".*" } }).toArray(function(err, contacts){
        if(err)  deferred.reject("Updating error");
        deferred.resolve(contacts);
    });

    return deferred.promise;
}

function addContact(contactParams) {
    var deferred = Q.defer();

    db.contacts.findOne(
        { name: contactParams.name },
        function (err, contact) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            if (contact) {
                deferred.reject('Contact "' + contactParams.name + '" already exists');
            } else {
                createContact();
            }
        });

    function createContact() {
        var contact = _.omit({ name : contactParams.name,
            phone : contactParams.phone,
            image : contactParams.image,
            id: (Math.floor((Math.random()*10000000))).toString() });

        db.contacts.insert(
            contact,
            function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);

                deferred.resolve();
            });
    }

    return deferred.promise;
}

function editContact(_id, replace) {
    var deferred = Q.defer();


    db.contacts.findOne(
        { name: replace.name },
        function (err, contact) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            if (contact) {
                deferred.reject('Contact "' + replace.name + '" already exists');
                console.log('Contact "' + replace.name + '" already exists');
            } else {
                db.contacts.findOneAndUpdate({id : _id}, replace, {upsert:true}, function(err, doc){
                    if (err) deferred.reject();
                    deferred.resolve();
                });
            }
        });
    deferred.resolve();

    return deferred.promise;
}

function delContact(_id) {
    var deferred = Q.defer();

    db.contacts.remove({ id: _id}, function(err, doc){
        if (err) deferred.reject();
        deferred.resolve();
    });

    return deferred.promise;
}

function populateDB(){
    var contact1 = { name: "Shlomi Malca", phone : "0507854165", image : "0" };
    var contact2 = { name: "Itamar Oren",  phone : "0507839768", image : "0" };
    var contact3 = { name: "Guy Gazit",    phone : "0507896525", image : "0" };
    var contact4 = { name: "Roe Aaron",    phone : "0534894152", image : "0" };
    var contact5 = { name: "Alder Faran",  phone : "0548981352", image : "0" };
    var contact6 = { name: "May Shamir",   phone : "0521486812", image : "0" };
    var contact7 = { name: "Eyal Ackerman",phone : "0532168189", image : "0" };
    var contact8 = { name: "Gil Avramov",  phone : "0538791563", image : "0" };
    var contact9 = { name: "Gilad Polak",  phone : "0533518166", image : "0" };
    var contact10 = { name: "Roe Klein",    phone : "0528666132", image : "0" };
    var contact11 = { name: "Adam Cohen",   phone : "0531684865", image : "0" };
    var contact12 = { name: "Barak Hadad",  phone : "0535684184", image : "0" };


    createContact(contact1);
    createContact(contact2);
    createContact(contact3);
    createContact(contact4);
    createContact(contact5);
    createContact(contact6);
    createContact(contact7);
    createContact(contact8);
    createContact(contact9);
    createContact(contact10);
    createContact(contact11);
    createContact(contact12);

    function createContact(contactParams) {


        db.contacts.findOne(
            { name: contactParams.name },
            function (err, contact) {
                if (err) deferred.reject(err.name + ': ' + err.message);

                if (contact) {

                } else {
                    var contact = _.omit({ name : contactParams.name,
                        phone : contactParams.phone,
                        image : contactParams.image,
                        id: (Math.floor((Math.random()*10000000))).toString() });

                    db.contacts.insert(contact);
                }
            });
    }
};
populateDB();


module.exports = service;
