"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var index_1 = require("../_services/index");
var $ = require("jquery");
var TopPanelComponent = /** @class */ (function () {
    function TopPanelComponent(contactService) {
        this.contactService = contactService;
        this.search = new core_1.EventEmitter();
    }
    TopPanelComponent.prototype.onSearch = function (filter) {
        this.search.emit(filter);
    };
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], TopPanelComponent.prototype, "search", void 0);
    TopPanelComponent = __decorate([
        core_1.Component({
            selector: 'top_panel',
            templateUrl: './app/top_panel/top_panel.component.html',
            styleUrls: ['./app/top_panel/top_panel.component.css']
        }),
        __metadata("design:paramtypes", [index_1.ContactsService])
    ], TopPanelComponent);
    return TopPanelComponent;
}());
exports.TopPanelComponent = TopPanelComponent;
//# sourceMappingURL=top_panel.component.js.map