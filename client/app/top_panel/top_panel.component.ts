
import { Component, Output, EventEmitter } from '@angular/core';

import { ContactsService } from '../_services/index';
import {Contact} from "../_models/contact";

declare module "jquery" {
    export = $;
}
import * as $ from "jquery";

@Component({
    selector: 'top_panel',
    templateUrl: './app/top_panel/top_panel.component.html',
    styleUrls: ['./app/top_panel/top_panel.component.css']
})

export class TopPanelComponent {

    @Output() search = new EventEmitter<string>();

    constructor(private contactService: ContactsService) {
    }

    onSearch(filter){
        this.search.emit(filter);
    }

}

