import { Component, ViewChild } from '@angular/core';
import {TableComponent} from "./table/table.component";

@Component({
    selector: 'center_panel',
    templateUrl: './app/center_panel/center_panel.component.html',
    styleUrls: ['./app/center_panel/center_panel.component.css']
})

export class CenterPanelComponent {

    @ViewChild(TableComponent) table;

    ngAfterViewInit() {
    }

    search(filter){
        this.table.search(filter);
    }
}