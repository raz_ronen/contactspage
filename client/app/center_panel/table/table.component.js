"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var index_1 = require("../../_services/index");
var TableComponent = /** @class */ (function () {
    function TableComponent(contactService) {
        this.contactService = contactService;
        this.contacts = [];
        this.isAddPanelVisible = false;
        this.isCallingVisible = false;
        this.filteredResult = false;
        this.filter = "";
        this.editing = false;
        this.currentContact = JSON.parse(localStorage.getItem('currentContact'));
    }
    TableComponent.prototype.ngOnInit = function () {
        document.getElementById("table").style.zIndex = "1";
        this.setAddPanelZIndex();
        this.getAll();
    };
    TableComponent.prototype.getAll = function () {
        var _this = this;
        this.contactService.getAll().subscribe(function (contacts) { _this.filteredResult = false; _this.contacts = contacts; });
    };
    TableComponent.prototype.addContact = function (name, phone) {
        var _this = this;
        this.setAddPanelZIndex();
        if (this.editing) {
            this.editContactProcedure(name, phone);
            return;
        }
        this.editing = false;
        if (name == "") {
            alert("Please insert a name.");
            return;
        }
        if (!isValid(phone)) {
            alert("Please insert a valid phone number.\nPhone number with 10 digits\n example: 0503390100");
            return;
        }
        this.isAddPanelVisible = false;
        var params = { "name": name, "phone": phone, "image": "" };
        this.contactService.add(params).subscribe(function (data) { _this.getAll(); }, function (error) { alert("Contact '" + name + "' already exists."); });
        function isValid(p) {
            var phoneRe = /^[0-9]\d{2}[0-9]\d{2}\d{4}$/;
            var digits = p.replace(/\D/g, "");
            return phoneRe.test(digits);
        }
    };
    TableComponent.prototype.setAddPanelZIndex = function () {
        if (this.isAddPanelVisible) {
            document.getElementById("addpanel").style.zIndex = "2";
        }
        else {
            document.getElementById("addpanel").style.zIndex = "1";
        }
        if (this.isCallingVisible) {
            document.getElementById("callpanel").style.zIndex = "2";
        }
        else {
            document.getElementById("callpanel").style.zIndex = "1";
        }
    };
    TableComponent.prototype.editContactProcedure = function (name, phone) {
        var _this = this;
        this.editing = false;
        this.editContact.name = name;
        this.editContact.phone = phone;
        this.contactService.editContact(this.editContact).subscribe(function (data) { _this.isAddPanelVisible = false; _this.getAll(); }, function (error) { alert("error"); });
    };
    TableComponent.prototype.ClearFields = function () {
        //noinspection TypeScriptUnresolvedVariable
        document.getElementById("add_name").value = "";
        //noinspection TypeScriptUnresolvedVariable
        document.getElementById("add_phone").value = "";
    };
    TableComponent.prototype.search = function (filter) {
        var _this = this;
        this.contactService.getAllByFilter(filter).subscribe(function (contacts) {
            if (filter != "")
                _this.filteredResult = true;
            _this.filter = filter;
            _this.contacts = contacts;
        });
    };
    TableComponent.prototype.PlusPressed = function () {
        this.isCallingVisible = false;
        this.ClearFields();
        if (this.editing) {
            this.editing = false;
            return;
        }
        this.isAddPanelVisible = !this.isAddPanelVisible;
        this.editing = false;
        this.setAddPanelZIndex();
    };
    TableComponent = __decorate([
        core_1.Component({
            selector: 'contacts_table',
            templateUrl: './app/center_panel/table/table.component.html',
            styleUrls: ['./app/center_panel/table/table.component.css']
        }),
        __metadata("design:paramtypes", [index_1.ContactsService])
    ], TableComponent);
    return TableComponent;
}());
exports.TableComponent = TableComponent;
//# sourceMappingURL=table.component.js.map