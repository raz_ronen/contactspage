import { Component } from '@angular/core';

import { Contact } from '../../_models/index';
import { ContactsService } from '../../_services/index';

@Component({
    selector: 'contacts_table',
    templateUrl: './app/center_panel/table/table.component.html',
    styleUrls: ['./app/center_panel/table/table.component.css']
})

export class TableComponent {
    currentContact: Contact;
    contacts: Contact[] = [];
    isAddPanelVisible: boolean = false;
    isCallingVisible: boolean = false;
    filteredResult: boolean = false;
    filter: string = "";
    editing: boolean = false;
    editContact: Contact;

    constructor(private contactService: ContactsService) {
        this.currentContact = JSON.parse(localStorage.getItem('currentContact'));

    }

    ngOnInit(){
        document.getElementById("table").style.zIndex = "1";
        this.setAddPanelZIndex();
        this.getAll();
    }

    private getAll(){
        this.contactService.getAll().subscribe(contacts => {this.filteredResult = false;this.contacts = contacts;});
    }

    private addContact(name,phone){
        this.setAddPanelZIndex();
        if(this.editing) { this.editContactProcedure(name,phone); return; }
        this.editing = false;
        if(name=="") {
            alert("Please insert a name.");
            return;
        }
        if(!isValid(phone)) {
            alert("Please insert a valid phone number.\nPhone number with 10 digits\n example: 0503390100");
            return;
        }
        this.isAddPanelVisible = false;

        var params = { "name" : name , "phone" : phone,"image":""}
        this.contactService.add(params).subscribe(data => {this.getAll();},
            error => {alert("Contact '" + name + "' already exists.");});

        function isValid(p) {
            var phoneRe = /^[0-9]\d{2}[0-9]\d{2}\d{4}$/;
            var digits = p.replace(/\D/g, "");
            return phoneRe.test(digits);
        }
    }

    setAddPanelZIndex(){
        if(this.isAddPanelVisible) {
            document.getElementById("addpanel").style.zIndex = "2";
        } else {
            document.getElementById("addpanel").style.zIndex = "1";
        }
        if(this.isCallingVisible) {
            document.getElementById("callpanel").style.zIndex = "2";
        } else {
            document.getElementById("callpanel").style.zIndex = "1";
        }
    }


    editContactProcedure(name, phone){
        this.editing = false;
        this.editContact.name = name;
        this.editContact.phone = phone;
        this.contactService.editContact(this.editContact).subscribe(data => {this.isAddPanelVisible = false; this.getAll();},
            error => {alert("error");});
    }

    private ClearFields(){
        //noinspection TypeScriptUnresolvedVariable
        document.getElementById("add_name").value = "";
        //noinspection TypeScriptUnresolvedVariable
        document.getElementById("add_phone").value = "";
    }

    search(filter){
        this.contactService.getAllByFilter(filter).subscribe((contacts : Contact[]) => {
            if(filter!="") this.filteredResult = true;this.filter=filter; this.contacts = contacts;});
    }

    PlusPressed(){
        this.isCallingVisible = false;
        this.ClearFields();
        if(this.editing) {this.editing = false; return;}
        this.isAddPanelVisible = !this.isAddPanelVisible;
        this.editing = false;
        this.setAddPanelZIndex();
    }

}