import { Component, Input, Output, EventEmitter } from '@angular/core';

import { Contact } from '../../../_models/index';
import { ContactsService } from '../../../_services/index';

@Component({
    selector: '[my-tr]',
    templateUrl: './app/center_panel/table/table_row/table_row.component.html',
})

export class TableRowComponent {

    @Input('contact') contact: Contact;
    @Input('filteredResult') filteredResult: boolean;
    @Input('filter') filter: string;
    @Output() onDeleted : EventEmitter<any>= new EventEmitter();
    @Output() onEdit : EventEmitter<any>= new EventEmitter();
    @Output() onCall : EventEmitter<any>= new EventEmitter();
    calling : boolean = false;


    constructor(private contactService: ContactsService) {
    }

    ngOnInit(){
        if(this.filteredResult==true && this.filter!=""){
            var regular = this.contact.name.split(this.filter);
            var result = "";
            for(var i = 0; i < regular.length; i++){
                result += regular[i];
                if(i!=regular.length-1) result += "<b>" + this.filter + "</b>";
            }
            this.contact.name = result;
        }
    }

    delete() {
        this.contactService.delete(this.contact.id).subscribe(data => {this.onDeleted.emit();});
    }

    edit(){
        this.onEdit.emit(this.contact);
    }

    onRowClick(){
        this.onCall.emit(this.contact);
    }
}