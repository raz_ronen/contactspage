"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var index_1 = require("../../../_models/index");
var index_2 = require("../../../_services/index");
var TableRowComponent = /** @class */ (function () {
    function TableRowComponent(contactService) {
        this.contactService = contactService;
        this.onDeleted = new core_1.EventEmitter();
        this.onEdit = new core_1.EventEmitter();
        this.onCall = new core_1.EventEmitter();
        this.calling = false;
    }
    TableRowComponent.prototype.ngOnInit = function () {
        if (this.filteredResult == true && this.filter != "") {
            var regular = this.contact.name.split(this.filter);
            var result = "";
            for (var i = 0; i < regular.length; i++) {
                result += regular[i];
                if (i != regular.length - 1)
                    result += "<b>" + this.filter + "</b>";
            }
            this.contact.name = result;
        }
    };
    TableRowComponent.prototype.delete = function () {
        var _this = this;
        this.contactService.delete(this.contact.id).subscribe(function (data) { _this.onDeleted.emit(); });
    };
    TableRowComponent.prototype.edit = function () {
        this.onEdit.emit(this.contact);
    };
    TableRowComponent.prototype.onRowClick = function () {
        this.onCall.emit(this.contact);
    };
    __decorate([
        core_1.Input('contact'),
        __metadata("design:type", index_1.Contact)
    ], TableRowComponent.prototype, "contact", void 0);
    __decorate([
        core_1.Input('filteredResult'),
        __metadata("design:type", Boolean)
    ], TableRowComponent.prototype, "filteredResult", void 0);
    __decorate([
        core_1.Input('filter'),
        __metadata("design:type", String)
    ], TableRowComponent.prototype, "filter", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], TableRowComponent.prototype, "onDeleted", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], TableRowComponent.prototype, "onEdit", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], TableRowComponent.prototype, "onCall", void 0);
    TableRowComponent = __decorate([
        core_1.Component({
            selector: '[my-tr]',
            templateUrl: './app/center_panel/table/table_row/table_row.component.html',
        }),
        __metadata("design:paramtypes", [index_2.ContactsService])
    ], TableRowComponent);
    return TableRowComponent;
}());
exports.TableRowComponent = TableRowComponent;
//# sourceMappingURL=table_row.component.js.map