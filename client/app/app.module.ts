import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { TopPanelComponent } from './top_panel/top_panel.component';
import { CenterPanelComponent } from './center_panel/center_panel.component';
import { TableComponent } from './center_panel/table/table.component';
import { TableRowComponent } from './center_panel/table/table_row/table_row.component';
import {ContactsService} from "./_services/contact.service";

@NgModule({
    imports: [
        BrowserModule,
        HttpModule
    ],
    declarations: [
        AppComponent,
        TopPanelComponent,
        CenterPanelComponent,
        TableComponent,
        TableRowComponent
    ],
    providers: [
        ContactsService
    ],
    bootstrap: [AppComponent]
})

export class AppModule { }