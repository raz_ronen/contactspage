export class Contact {
    id: string;
    name: string;
    phone: string;
    image: string;
}