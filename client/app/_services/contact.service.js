"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/map");
var ContactsService = /** @class */ (function () {
    function ContactsService(http) {
        this.http = http;
        //ip : string = '51.140.184.133';
        this.ip = 'localhost';
    }
    ContactsService.prototype.getAll = function () {
        //noinspection TypeScriptUnresolvedFunction
        return this.http.get('http://' + this.ip + ':4000/contacts/getAll').map(function (response) { return response.json(); });
    };
    ContactsService.prototype.getAllByFilter = function (_filter) {
        var params = { "filter": _filter };
        //noinspection TypeScriptUnresolvedFunction
        return this.http.post('http://' + this.ip + ':4000/contacts/displayByFilter', params).map(function (response) { return response.json(); });
    };
    ContactsService.prototype.add = function (params) {
        return this.http.post('http://' + this.ip + ':4000/contacts/add', params);
    };
    ContactsService.prototype.editContact = function (contact) {
        var params = { "name": contact.name, "phone": contact.phone, "image": contact.image, "id": contact.id };
        return this.http.post('http://' + this.ip + ':4000/contacts/edit/' + contact.id, params);
    };
    ContactsService.prototype.delete = function (_id) {
        return this.http.delete('http://' + this.ip + ':4000/contacts/delContact/' + _id);
    };
    ContactsService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.Http])
    ], ContactsService);
    return ContactsService;
}());
exports.ContactsService = ContactsService;
//# sourceMappingURL=contact.service.js.map