import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'
import { Contact } from '../_models/index';


@Injectable()
export class ContactsService {
    //ip : string = '51.140.184.133';
    ip : string = 'localhost';
    constructor(private http: Http) { }

    getAll() {
        //noinspection TypeScriptUnresolvedFunction
        return this.http.get('http://' + this.ip + ':4000/contacts/getAll').map((response: Response) => response.json());
    }

    getAllByFilter(_filter: string) {
        var params = { "filter" : _filter};
        //noinspection TypeScriptUnresolvedFunction
        return this.http.post('http://' + this.ip + ':4000/contacts/displayByFilter', params).map((response: Response) => response.json());
    }

    add(params) {
        return this.http.post('http://' + this.ip + ':4000/contacts/add', params);
    }

    editContact(contact: Contact) {
        var params = { "name" : contact.name, "phone" : contact.phone, "image" : contact.image, "id": contact.id };
        return this.http.post('http://' + this.ip + ':4000/contacts/edit/' + contact.id, params);
    }

    delete(_id: string) {
        return this.http.delete('http://' + this.ip + ':4000/contacts/delContact/' + _id);
    }
}